# simple_koa_ts

#### 介绍
一个简单的koa使用ts的基础项目

#### 使用的npm库

- koa
- koa-static
- koa-body
- koa-router
- koa2-cors


#### 使用方法

```bash
$ npm i
$ npm start
```

#### 访问网址

测试地址: [http://localhost:1234/](http://localhost:1234/)