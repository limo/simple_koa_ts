import Koa from 'koa';
import http from 'http';
import path from 'path';
import Router from 'koa-router';
import koaStatic from 'koa-static';
import cors from 'koa2-cors';
import koaBody from 'koa-body';
import mainRouter from './routes/main';

const app = new Koa();
const router = new Router();
router.use('/', mainRouter.routes());
app.use(cors());
app.use(koaBody());
app.use(koaStatic(path.resolve(__dirname, '../public')));
app.use(router.routes());
app.use(router.allowedMethods());

const server = http.createServer(app.callback());

server.listen('1234', () => {
    console.log('server is on : http://127.0.0.1:1234')
})