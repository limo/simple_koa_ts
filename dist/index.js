"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const http_1 = __importDefault(require("http"));
const path_1 = __importDefault(require("path"));
const koa_router_1 = __importDefault(require("koa-router"));
const koa_static_1 = __importDefault(require("koa-static"));
const koa2_cors_1 = __importDefault(require("koa2-cors"));
const koa_body_1 = __importDefault(require("koa-body"));
const main_1 = __importDefault(require("./routes/main"));
const app = new koa_1.default();
const router = new koa_router_1.default();
router.use('/', main_1.default.routes());
app.use(koa2_cors_1.default());
app.use(koa_body_1.default());
app.use(koa_static_1.default(path_1.default.resolve(__dirname, '../public')));
app.use(router.routes());
app.use(router.allowedMethods());
const server = http_1.default.createServer(app.callback());
server.listen('1234', () => {
    console.log('server is on : http://127.0.0.1:1234');
});
